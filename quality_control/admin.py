from django.contrib import admin
from .models import Project, Task, BugReport, FeatureRequest


# Inline Class для модели Task
class TaskInline(admin.TabularInline):
    model = Task
    extra = 0
    fields = ('name', 'description', 'assignee', 'status', 'created_at', 'updated_at')
    readonly_fields = ('created_at', 'updated_at')
    can_delete = True
    show_change_link = True
# Inline Class для модели BugReport
class BagReportInline(admin.TabularInline):
    model = BugReport
    extra = 0
    fields = ('name', 'description', 'priority', 'status', 'created_at', 'updated_at')
    readonly_fields = ('created_at', 'updated_at')
    can_delete = True
    show_change_link = True

# Register your models here.
@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'created_at')  # какие поля будут отображаться в виде списка объектов
    search_fields = ('name', 'description')  # поисковая строка для фильтрации списка объектов
    ordering = ('created_at',)
    date_hierarchy = 'created_at'
    inlines = [TaskInline, BagReportInline]


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('name', 'project', 'assignee', 'status', 'created_at', 'updated_at')
    list_filter = ('status', 'assignee', 'project')
    search_fields = ('name', 'description')
    list_editable = ('status', 'assignee')
    readonly_fields = ('created_at', 'updated_at')


@admin.register(BugReport)
class BugReportAdmin(admin.ModelAdmin):
    list_display = ('title', 'project', 'task', 'status', 'created_at', 'updated_at')
    list_filter = ('status', 'project', 'task')
    search_fields = ('title', 'description')
    list_editable = ('status',)
    readonly_fields = ('created_at', 'updated_at')
    fieldsets = [('Main Info', {'fields': ['title', 'description']}),
                 ('Info About Project', {'fields': ['project', 'task', 'status']}),
                 ('Additional Info', {'fields': ['created_at', 'updated_at']})]


@admin.register(FeatureRequest)
class FeatureRequestAdmin(admin.ModelAdmin):
    list_display = ('title', 'project', 'task', 'status', 'priority', 'created_at', 'updated_at')
    list_filter = ('status', 'project', 'task', 'priority')
    search_fields = ('title', 'description')
    list_editable = ('status',)
    readonly_fields = ('created_at', 'updated_at')
    fieldsets = [('Main Info', {'fields': ['title', 'description']}),
                 ('Info About Project', {'fields': ['project', 'task', 'status', 'priority']}),
                 ('Additional Info', {'fields': ['created_at', 'updated_at']})]
    #filter_vertical = ('priority',)
