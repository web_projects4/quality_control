from django.urls import path
from quality_control import views

app_name = 'quality_control'

urlpatterns = [
    #path('', views.index, name='index'),
    #path('bug_list/', views.bug_list, name='bug_list'),
    #path('bug_list/<id>/', views.bug_detail, name='bug_detail'),
    #path('feature_list/', views.feature_list, name='feature_list'),
    #path('feature_list/<id>/', views.feature_id_detail, name='feature_id_detail')
    path('', views.IndexView.as_view(), name='index'),

    path('bug_list/', views.BugsListView.as_view(), name='bug_list'),
    path('bug_list/<id>/', views.BugsDetailView.as_view(), name='bug_detail'),
    path('feature_list/', views.FeatureListView.as_view(), name='feature_list'),
    path('feature_list/<id>/', views.FeatureDetailView.as_view(), name='feature_detail'),

    path('bug_list/add_bug/', views.BugReportCreateView.as_view(), name='create_bug'),
    path('feature_list/add_feature/', views.FeatureRequestCreateView.as_view(), name='create_feature'),

    path('bug_list/<bug_id>/update/', views.BugReportUpdateView.as_view(), name='update_bug'),
    path('bug_list/<bug_id>/delete/', views.BugReportDeleteView.as_view(), name='delete_bug'),
    path('feature_list/<feature_id>/update/', views.FeatureRequestUpdateView.as_view(), name='update_feature'),
    path('features_list/<feature_id>/delete/', views.FeatureRequestDeleteView.as_view(), name='delete_feature'),
]