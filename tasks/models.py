from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Project(models.Model):
    name = models.CharField(max_length=100) # Хранение названия проекта
    description = models.TextField() # Хранение описания проекта
    created_at = models.DateTimeField(auto_now_add=True) # Автоматически устанавливает дату и время при создании проекта

    def __str__(self):
        return self.name
class Task(models.Model):
    STATUS_CHOICES = [
        ('New', 'Новая'),
        ('In_progress', 'В работе'),
        ('Completed', 'Завершена'),
    ]
    # отношение один ко многим с моделью project -> каждая задача связана с конкретным проектом
    project = models.ForeignKey(
        Project,
        related_name='tasks', # позволяет обращаться через атрибут tasks
        on_delete=models.CASCADE # при удалении проекта все связанные с ним задачи также будут удалены
    )
    name = models.CharField(max_length=100) # Название/заголовок задачи
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True) # При создании объекта будет установлена текущая дата и время - auto_now_add
    updated_at = models.DateTimeField(auto_now=True) # Обновляем время при сохранении
    assignee = models.ForeignKey( # Указывает на пользователя
        User,
        related_name='tasks',
        on_delete=models.SET_NULL, # в случае удаления устанавливается в NULL
        null=True,
        blank=True
    )
    # статус задач для отслеживания
    status = models.CharField(
        max_length=50,
        choices=STATUS_CHOICES, # ограниченный набор опций
        default='New',
    )
